document.addEventListener("DOMContentLoaded", function () {
    const pricingButtons = document.querySelectorAll(".btn-pricing");

    pricingButtons.forEach(function (button, index) {
        button.addEventListener("click", function () {
            setSliderValue(index);
        });
    });

    function setSliderValue(index) {
        const slider = document.getElementById("userSlider");
        if (slider) {
            const sliderValue = (index + 1) * 10;
            slider.value = sliderValue;
        }
    }

    const userSlider = document.getElementById("userSlider");
    if (userSlider) {
        userSlider.addEventListener("input", function () {
            const sliderValue = parseInt(userSlider.value);

            pricingButtons.forEach(function (button, index) {
                const minRange = index * 10;
                const maxRange = (index + 1) * 10;

                if (sliderValue >= minRange && sliderValue <= maxRange) {
                    button.classList.add("btn-primary");
                } else {
                    button.classList.remove("btn-primary");
                }
            });
        });
    }
});
$(document).ready(function () {
    setTimeout(function () {
        $(".pricing-card:nth-child(odd)").css({
            transform: "translateX(0)",
            opacity: 1
        });

        $(".pricing-card:nth-child(even)").css({
            transform: "translateX(0)",
            opacity: 1
        });

        $(".pricing-card:nth-child(3)").css({
            transform: "translateY(0)",
            opacity: 1
        });

        // Animate the H1 element
        $("h1").css({
            transform: "scale(1)",
            opacity: 1,
            fontSize: "36px"
        });
    }, 1000); 
});
function populateForm() {
    var nameInput = document.getElementById("name");
    var emailInput = document.getElementById("email");
    var commentsInput = document.getElementById("comments");

    if (nameInput && emailInput && commentsInput) {
        nameInput.value = "John Doe";
        emailInput.value = "johndoe@example.com";
        commentsInput.value = "This is a test order.";
    }
}


//  Section B: Lazy Loading To Avoid Pagination (vanilla js)
let isLoading = false;
let page = 1;

function loadMoreContent() {
    if (isLoading) return;
    isLoading = true;

  
    setTimeout(() => {
        const newData = generateRandomData(); 

 
        newData.forEach(item => {
            const newItem = document.createElement("div");
            newItem.classList.add("item");
            newItem.textContent = item;
            document.getElementById("content").appendChild(newItem);
        });

        isLoading = false;
        page++;
    }, 1000);
}

function generateRandomData() {
    const newData = [];
    for (let i = 1; i <= 10; i++) {
        newData.push(`Item ${page * 10 + i}`);
    }
    return newData;
}

window.addEventListener("scroll", () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        loadMoreContent();
    }
});

loadMoreContent();
